#include <iostream>
#include <string>

using namespace std;

struct student {
private:
	string l_name;
	string f_name;
	float avgAssessment;
public:
	int age;
	student(string l_name, string f_name, int age, float avgAssessment) {
		this->l_name = l_name;
		this->f_name = f_name;
		this->age = age;
		this->avgAssessment = avgAssessment;
	}
	student() {
		this->l_name = "-";
		this->f_name = "-";
		this->age = 0;
		this->avgAssessment = 0;
	}
	void show() {
		cout << "Имя: " << l_name  << "\n"<< "Фамилия: " << f_name << "\n" << "Возраст: " << age << "\n" << "Средний балл: " << avgAssessment << "\n" <<endl;
	}

	string search(string search) {
			return l_name;		
	}

	bool searchBool(string search) {
		if (this->l_name == search) {
			return true;
		}
		else {
			return false;
		}
	}
};

int main() {
	setlocale(LC_ALL, "ru");
	static constexpr size_t MAX_SIZE = 10;
	student students[MAX_SIZE]{
		student("Ilya", "Logvinenko", 18, 3),
		student("Ivan", "Skinder", 16, 5),
		student("Djabbarov", "Ibragim", 15, 4.5),
		student("Nikita", "Simonenko", 14, 4.5),
		student("Maksim", "Terehov", 21, 4.5),
		student("Lexa", "Ribakov", 23, 4.5),
		student("Dima", "Shiba", 12, 4.5),
		student("Vlad", "Vlad", 18, 4.5),
		student("Artem", "Har", 23, 4.5),
		student("Nastya", "Chmeleva", 33, 4.5)
	};

	cout << "Студенты колледжа" << endl;

	for (size_t i = 0; i < size(students); ++i) {
		students[i].show();
	}
	int ageMin = students[0].age;
	int ageMax = students[0].age;
	for (size_t i = 0; i < size(students); ++i) {
		int s = students[i].age;
		if (s <= ageMin) {
			ageMin = s;
		}
		if (s >= ageMax) {
			ageMax = s;
		}
	}
	cout << "Минимальный возраст в колледже" <<ageMin << endl;
	cout << "Максимальный возраст в колледже" << ageMax << endl;
	
	string search;
	string o;
	cout << "Введите любое имя" << endl;
	cin >> search;
	for (size_t i = 0; i < size(students); ++i) {
		if (students[i].searchBool(search)) {
			o = students[i].search(search);
				break;
		}
	}
	if (o != "") {
		cout << "Ответ : " << o << endl;
	}
	else {
		cout << "Ответ : " << "Не существует в данном списке!" << endl;
	}
	
	int r;
	cin >> r;
	return 0;
}